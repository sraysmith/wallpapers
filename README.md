# WELCOME ALL TO MY WALLPAPERS COLLECTION!
This is my personal collection of wallpapers that I grabbed up from [triple monitor backgrounds](https://www.triplemonitorbackgrounds.com/ "Triple Monitor Backgrounds") and [wallpaperfusion](https://www.wallpaperfusion.com/Search/?Monitors=3 "WallpaperFusion"). All of these wallpapers are 5760x1080. None of them are NSFW, so feel free to browse and grab what you want!

## WALLPAPERS
I have organized these wallpapers into the following categories:

- Abstract
- Anime
- Bridges
- Cityscapes
- Landscapes
- Nature
- Places
- Spacescapes
- Tech
- Texture
- Vehicles


